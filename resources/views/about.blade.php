@extends('layouts.main')

@section('content')
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>ABOUT <span>ME</span></h1>
    <span class="title-bg">Resume</span>
</section>
<!-- Page Title Ends -->

<!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <div class="row">

                <!-- Personal Info Starts -->
                <div class="col-12 col-lg-5 col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">personal information</h3>
                        </div>
                        <div class="col-12 d-block d-sm-none">
                            <img src="img/img-mobile.jpg" class="img-fluid main-img-mobile" alt="my picture" />
                        </div>
                        <div class="col-12">
                            <ul class="about-list list-unstyled open-sans-font">
                                <li> <span class="title">first name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Putu Adi</span> </li>
                                <li > <span class="title">last name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Bawa</span> </li>
                                <li> <span class="title">Age :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">22 Years</span> </li>
                                <li> <span class="title">Nationality :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                                <li> <span class="title">Freelance :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Available</span> </li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="about-list list-unstyled open-sans-font">
                                <li> <span class="title">Address :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Tunis</span> </li>
                                <li> <span class="title">phone :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">+21621184010</span> </li>
                                <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">you@mail.com</span> </li>
                                <li> <span class="title">Skype :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">steve.milner</span> </li>
                                <li> <span class="title">langages :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">French, English</span> </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
            <hr class="separator">
            <!-- Personal Info Ends -->


            <!-- Skills Starts -->
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">My Skills</h3>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p40">
                        <span>40%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">html</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p10">
                        <span>10%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">javascript</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">css</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p35">
                        <span>35%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">php</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p50">
                        <span>50%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Text Editor</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p30">
                        <span>30%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">MySql Database</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p90">
                        <span>90%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Typing Skills</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p20">
                        <span>20%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Testing</h6>
                </div>
            </div>
            <hr class="separator mt-1">
            <!-- Skills Ends -->

            <!-- Experience & Education Starts -->
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Experience <span>&</span> Education</h3>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume-box">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2021 - Present</span>
                                <h5 class="poppins-font text-uppercase">Ketua Dewan Kerja Cabang <span class="place open-sans-font">Kwartir Cabang Gerakan Pramuka Buleleng</span></h5>
                                <p class="open-sans-font">Menghimpun seluruh pramuka Tegak/Dega (SMA-seberajat/Mahasiswa) se-kabupaten Buleleng.</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2020/2021</span>
                                <h5 class="poppins-font text-uppercase">Ketua Umum <span class="place open-sans-font">UKM Pramuka Undiksha</span></h5>
                                <p class="open-sans-font">Salah satu organisasi mahasiswa di Universitas Pendidikan Ganesha.</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2019/2020</span>
                                <h5 class="poppins-font text-uppercase">Sekretaris II <span class="place open-sans-font">UKM Pramuka Undiksha</span></h5>
                                <p class="open-sans-font">Salah satu organisasi mahasiswa di Universitas Pendidikan Ganesha.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume-box">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2018 - Present</span>
                                <h5 class="poppins-font text-uppercase">Bachelor Degree <span class="place open-sans-font">Universitas Pendidikan Ganesha</span></h5>
                                <p class="open-sans-font">Program Studi Sistem Informasi dengan akreditasi B, di Universitas Pendidikan Ganesha dengan akreditas A.</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2015 - 2018</span>
                                <h5 class="poppins-font text-uppercase">Hight School <span class="place open-sans-font">SMK TI Bali Global Singaraja</span></h5>
                                <p class="open-sans-font">Jurusan Akuntansi dan Keuangan pada salah satu sekolah IT yang ada di kabupaten Buleleng</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2012 - 2015</span>
                                <h5 class="poppins-font text-uppercase">Junior Hight School<span class="place open-sans-font">SMP Negeri 2 Gerokgak</span></h5>
                                <p class="open-sans-font">Salah satu sekolah negeri di kecamatan Gerokgak-Buleleng</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2006 - 2012</span>
                                <h5 class="poppins-font text-uppercase">Elementary School<span class="place open-sans-font">SD Negeri 2 Banyupoh</span></h5>
                                <p class="open-sans-font">Salah satu SD negeri di desa Banyupoh-Gerokgak</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Experience & Education Ends -->
        </div>
    </section>
    <!-- Main Content Ends -->
@endsection
