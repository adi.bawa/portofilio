@extends('layouts.main')

@section('content')

    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>blog</span></h1>
        <span class="title-bg">posts</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <!-- Articles Starts -->
            <div class="row">
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post" class="d-block position-relative overflow-hidden">
                                <img src="img/blog/blog-post-1.jpg" class="img-fluid" alt="Blog Post">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post">How to Own Your Audience by Creating an Email List</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore...
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post" class="d-block position-relative overflow-hidden">
                                <img src="img/blog/blog-post-2.jpg" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post">Top 10 Toolkits for Deep Learning in 2020</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore...
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post" class="d-block position-relative overflow-hidden">
                                <img src="img/blog/blog-post-3.jpg" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post">Everything You Need to Know About Web Accessibility</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore...
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <!-- Articles Ends -->
        </div>

    </section>
@endsection